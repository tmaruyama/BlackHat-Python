#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import threading
import sys

def handle_client(client_socket):
    request = client_socket.recv(1024)
    print("[*] Received: %s" % request.decode('UTF-8'))
    client_socket.send(b"ACK!")
    client_socket.close()


def main():
    args = sys.argv
    if len(args) != 3:
        print("Usage: # python {} [host] [port]".format(args[0]))
        sys.exit()

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((args[1], int(args[2])))
    server.listen(5)
    print("[*] Listening on %s: %d" % (args[1], int(args[2])))
    
    while True:
        client, addr = server.accept()
        print("[*] Accepted connection form %s: %d" % (addr[0], addr[1]))
        client_handler = threading.Thread(target=handle_client, args=(client, ))
        client_handler.start()


if __name__ == '__main__':
    main()
