#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import socket


def main():
    args = sys.argv
    if len(args) != 3:
        print("Usage: # python {} [host] [port]".format(args[0]))
        sys.exit()

    target_host = args[1]
    target_port = int(args[2])

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((target_host, target_port))

    input_text = input('--> ')
    client.send(input_text.encode('UTF-8'))
    response = client.recv(4096)
    print(response.decode('UTF-8'))

if __name__ == '__main__':
    main()
